from django.contrib import admin

from .models import *
from .forms import *
# Register your models here.


class HistoriaInLine(admin.StackedInline):
	class Media:
		css = {
			'all': ('css/admin/test.css',)
		}
		
	model=Historia
	form = HistoriaAdminForm
	extra=1
	classes = ('grp-collapse grp-open',)
	inline_classes = ('grp-collapse grp-open',)
	fieldsets = (
		("Primer cop (edat)", {
			'fields': (
				('tabac_primer_cop',
				'oh_primer_cop',
				'thc_primer_cop',
				'coc_primer_cop',
				'amf_primer_cop',
				'her_primer_cop',
				'bzd_primer_cop'
				),),
			}),
		("Període màxima abstinencia (mesos)", {
			'fields': (
				('tabac_max_abstinencia',
				'oh_max_abstinencia',
				'thc_max_abstinencia',
				'coc_max_abstinencia',
				'amf_max_abstinencia',
				'her_max_abstinencia',
				'bzd_max_abstinencia'
				),),
			}),
		("Dies consum en els darrers 30 dies", {
			'fields': (
				('tabac_dias_consum',
				'oh_dias_consum',
				'thc_dias_consum',
				'coc_dias_consum',
				'amf_dias_consum',
				'her_dias_consum',
				'bzd_dias_consum',
				),),
			}),
		("Consum diari en grams", {
			'fields': (
				('tabac_consum_diari',
				'oh_consum_diari',
				'thc_consum_diari',
				'coc_consum_diari',
				'amf_consum_diari',
				'her_consum_diari',
				'bzd_consum_diari',
				),),
			}),
		("Via consum  actual (Via consum = 1-oral, 2-fumada, 3-inhalada, 4-esnifada, 5-injectada, 6-altres, 7-desconegut)", {
			'fields': (
				('tabac_via_consum',
				'oh_via_consum',
				'thc_via_consum',
				'coc_via_consum',
				'amf_via_consum',
				'her_via_consum',
				'bzd_via_consum',
				),),
			}),
		("Desintoxicació anterior", {
			'fields': (
				('tabac_detox_anterior',
				'oh_detox_anterior',
				'thc_detox_anterior',
				'coc_detox_anterior',
				'amf_detox_anterior',
				'her_detox_anterior',
				'bzd_detox_anterior',
				),),
			}),
		('Altres', {
			'fields': (
				('tox_ppal',
				'tus_altres'),
				('gravetat_tus',
				'projecte_tt',
				'reingres'),
				('cc_fisiques',
				'cc_emocionals'),
				'dg_alta',)
			})

		)



class EscalaInLine(admin.StackedInline):
	model=Escala
	extra=1
	inline_classes = ('grp-collapse grp-open',)
	fieldsets=(
		('Impulsivistat Barrat', {
			'fields': (
				('barrat_01',
				'barrat_02',
				'barrat_03',
				'barrat_04',
				'barrat_05'),
				('barrat_06',
				'barrat_07',
				'barrat_08',
				'barrat_09',
				'barrat_10'),
				('barrat_11',
				'barrat_12',
				'barrat_13',
				'barrat_14',
				'barrat_15'),
				('barrat_16',
				'barrat_17',
				'barrat_18',
				'barrat_19',
				'barrat_20'),
				('barrat_21',
				'barrat_22',
				'barrat_23',
				'barrat_24',
				'barrat_25'),
				('barrat_26',
				'barrat_27',
				'barrat_28',
				'barrat_29',
				'barrat_30'),)

			}),
		('Alexitimia Toronto', {
			'fields': (
    			('alex_01',
    			'alex_02',
    			'alex_03',
    			'alex_04',
    			'alex_05'),
    			('alex_06',
    			'alex_07',
    			'alex_08',
    			'alex_09',
    			'alex_10'),
    			('alex_11',
    			'alex_12',
    			'alex_13',
    			'alex_14',
    			'alex_15'),
    			('alex_16',
    			'alex_17',
    			'alex_18',
    			'alex_19',
    			'alex_20'),)
			}),
		('Autoestima Rosemberg', {
			'fields': (
				('rosemberg_01',
    			'rosemberg_02',
    			'rosemberg_03',
    			'rosemberg_04',
    			'rosemberg_05'),
    			('rosemberg_06',
    			'rosemberg_07',
    			'rosemberg_08',
    			'rosemberg_09',
    			'rosemberg_10'),)
			}),
		('Autoestigma (ISMI)', {
			'fields': (
    			('ismi_01',
    			'ismi_02',
    			'ismi_03',
    			'ismi_04',
    			'ismi_05'),
    			('ismi_06',
    			'ismi_07',
    			'ismi_08',
    			'ismi_09',
    			'ismi_10'),
    			('ismi_11',
    			'ismi_12',
    			'ismi_13',
    			'ismi_14',
    			'ismi_15'),
    			('ismi_16',
    			'ismi_17',
    			'ismi_18',
    			'ismi_19',
    			'ismi_20'),
    			('ismi_21',
    			'ismi_22',
    			'ismi_23',
    			'ismi_24',
    			'ismi_25'),
    			('ismi_26',
    			'ismi_27',
    			'ismi_28',
    			'ismi_29'),)
			}),
		('Valoració Insight Clinic', {
			'fields': (
    			('clinic_tus',
    			'clinic_neccesitat',
    			'clinic_social',
    			'clinic_fam'),
    			('clinic_fisiq',
    			'clinic_emo',
    			'clinic_diners'),)
			}),
		('Criteris DSM-IV', {
			'fields': (
    			('dsm_ctrl_1',
    			'dsm_ctrl_2',
    			'dsm_ctrl_3',
    			'dsm_ctrl_4'),
    			('dsm_ctrl_5',
    			'dsm_fr',
    			'dsm_conseq_1',
    			'dsm_conseq_2'),
    			('dsm_conseq_3',
    			'dsm_conseq_4',
    			'dsm_conseq_5'),)
			}),
		('Valoració Insight Cognitiu', {
			'fields': (
    			('ic_self',
    			'ic_internal'),)
			}),
		('Escala HANIL Ingrés', {
			'fields': (
    			('hamil_pre_01',
				'hamil_pre_02',
				'hamil_pre_03',
				'hamil_pre_04',
				'hamil_pre_05'),
				('hamil_pre_06',
				'hamil_pre_07',
				'hamil_pre_08',
				'hamil_pre_09',
				'hamil_pre_10'),
				('hamil_pre_11',
				'hamil_pre_12',
				'hamil_pre_13',
				'hamil_pre_14',
				'hamil_pre_15'),
				('hamil_pre_16',
				'hamil_pre_17',
				'hamil_pre_18',
				'hamil_pre_19',
				'hamil_pre_20'),)
			}),
		('Escala HANIL Alta', {
			'fields': (
    			('hamil_post_01',
				'hamil_post_02',
				'hamil_post_03',
				'hamil_post_04',
				'hamil_post_05'),
				('hamil_post_06',
				'hamil_post_07',
				'hamil_post_08',
				'hamil_post_09',
				'hamil_post_10'),
				('hamil_post_11',
				'hamil_post_12',
				'hamil_post_13',
				'hamil_post_14',
				'hamil_post_15'),
				('hamil_post_16',
				'hamil_post_17',
				'hamil_post_18',
				'hamil_post_19',
				'hamil_post_20'),)
			}),
		('Escala URICA', {
			'fields': (
    			('urica_01',
    			'urica_02',
    			'urica_03',
    			'urica_04',
    			'urica_05'),
    			('urica_06',
    			'urica_07',
    			'urica_08',
    			'urica_09',
    			'urica_10'),
    			('urica_11',
    			'urica_12',
    			'urica_13',
    			'urica_14',
    			'urica_15'),
    			('urica_16',
    			'urica_17',
    			'urica_18',
    			'urica_19',
    			'urica_20'),
    			('urica_21',
    			'urica_22',
    			'urica_23',
    			'urica_24',
    			'urica_25'),
    			('urica_26',
    			'urica_27',
    			'urica_28',
    			'urica_29',
    			'urica_30'),
    			('urica_31',
    			'urica_32'),)
			}),
		('Escala BCIS Ingrés', {
			'fields': (
    			('bcis_pre_01',
    			'bcis_pre_02',
    			'bcis_pre_03',
    			'bcis_pre_04',
    			'bcis_pre_05'),
    			('bcis_pre_06',
    			'bcis_pre_07',
    			'bcis_pre_08',
    			'bcis_pre_09',
    			'bcis_pre_10'),
    			('bcis_pre_11',
    			'bcis_pre_12',
    			'bcis_pre_13',
    			'bcis_pre_14',
    			'bcis_pre_15'),)
			}),
		('Escala BCIS Alta', {
			'fields': (
    			('bcis_post_01',
    			'bcis_post_02',
    			'bcis_post_03',
    			'bcis_post_04',
    			'bcis_post_05'),
    			('bcis_post_06',
    			'bcis_post_07',
    			'bcis_post_08',
    			'bcis_post_09',
    			'bcis_post_10'),
    			('bcis_post_11',
    			'bcis_post_12',
    			'bcis_post_13',
    			'bcis_post_14',
    			'bcis_post_15'),)
			}),
		('FRBSE pacient', {
			'fields': (
    			('frbse_pac_apa_pre',
    			'frbse_pac_apa_post',
    			'frbse_pac_desinh_pre'),
    			('frbse_pac_desinh_post',
    			'frbse_pac_exe_pre',
    			'frbse_pac_exe_post'),)
			}),
		('FRBSE informant', {
			'fields': (
    			('frbse_inf_apa_pre',
    			'frbse_inf_apa_post',
    			'frbse_inf_desinh_pre'),
    			('frbse_inf_desinh_post',
    			'frbse_inf_exe_pre',
    			'frbse_inf_exe_post'),)
			}),
		('BDI', {
			'fields': (
    			('bdi_pre',
    			'bdi_post'),)
    	}),
		
		('BAI', {
			'fields': (
    			('bai_pre',
    			'bai_post'),)
    	}),
		('MADRS', {
			'fields': (
    			('madrs_pre',
    			'madrs_post'),)
    	}),
		('BPRS', {
			'fields': (
    			('bprs_pos_pre',
    			'bprs_pos_post',
    			'bprs_neg_pre',
    			'bprs_neg_post'),)
    	}),
		('HONOS', {
			'fields': (
    			('honos_pre',
    			'honos_post'),)
    	}), 
		('Escala MANHEIM', {
			'fields': (
    			('manheim_pre_1_a_12',
    			'manheim_pre_13',
    			'manheim_pre_14',
    			'manheim_pre_15',
    			'manheim_pre_16'),
    			('manheim_post_1_a_12',
    			'manheim_post_13',
    			'manheim_post_14',
    			'manheim_post_15',
    			'manheim_post_16'),)
    	}),
    	('Questionario breve de confianza situacional', {
			'fields': (
    			('qbcs_01',
    			'qbcs_02',
    			'qbcs_03',
    			'qbcs_04'),
    			('qbcs_05',
    			'qbcs_06',
    			'qbcs_07',
    			'qbcs_08'),)
    	}),
		('Altres', {
			'fields': (
				('trauma',
    			'sintomas',
    			'covid'),
				('satis_01',
    			'satis_02',
    			'satis_03',
    			'satis_04'),
    			('satis_05',
    			'satis_06',
    			'satis_07',
    			'satis_08'),)
		}),
	)


@admin.register(Demo)
class DemoAdmin(admin.ModelAdmin):
	fields = (
		'codi_pacient',
		('edat_ingres',
		'genere',
		'estat_civil'),
		('nucli_conv',
		'estat_laboral',
		'nivel_instruccio'),)
	inlines = [
		HistoriaInLine,
		EscalaInLine
		]

@admin.register(Codi_pacient)
class Codi_pacientAdmin(admin.ModelAdmin):
	list_display = ('cip_pacient', 'codi_pacient')

