from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
import hashlib


class Codi_pacient(models.Model):
    cip_pacient = models.CharField(max_length=14)
    
    @property
    def codi_pacient(self):
      return hashlib.md5(self.cip_pacient.encode('utf-8')).hexdigest()



class Demo(models.Model):
    codi_pacient = models.CharField(max_length=100)
    edat_ingres = models.IntegerField(blank=True)
    genere = models.IntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
            ]
        )
    estat_civil = models.IntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(4)
            ]
        )
    nucli_conv = models.IntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(5)
            ]
        )
    estat_laboral = models.IntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(6)
            ]
        )
    nivel_instruccio = models.IntegerField(
        blank=True,
        null=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(6)
            ]
        )



#      .S    S.    .S    sSSs  sdSS_SSSSSSbs    sSSs_sSSs     .S_sSSs     .S   .S_SSSs
#     .SS    SS.  .SS   d%%SP  YSSS~S%SSSSSP   d%%SP~YS%%b   .SS~YS%%b   .SS  .SS~SSSSS
#     S%S    S%S  S%S  d%S'         S%S       d%S'     `S%b  S%S   `S%b  S%S  S%S   SSSS
#     S%S    S%S  S%S  S%|          S%S       S%S       S%S  S%S    S%S  S%S  S%S    S%S
#     S%S SSSS%S  S&S  S&S          S&S       S&S       S&S  S%S    d*S  S&S  S%S SSSS%S
#     S&S  SSS&S  S&S  Y&Ss         S&S       S&S       S&S  S&S   .S*S  S&S  S&S  SSS%S
#     S&S    S&S  S&S  `S&&S        S&S       S&S       S&S  S&S_sdSSS   S&S  S&S    S&S
#     S&S    S&S  S&S    `S*S       S&S       S&S       S&S  S&S~YSY%b   S&S  S&S    S&S
#     S*S    S*S  S*S     l*S       S*S       S*b       d*S  S*S   `S%b  S*S  S*S    S&S
#     S*S    S*S  S*S    .S*P       S*S       S*S.     .S*S  S*S    S%S  S*S  S*S    S*S
#     S*S    S*S  S*S  sSS*S        S*S        SSSbs_sdSSS   S*S    S&S  S*S  S*S    S*S
#     SSS    S*S  S*S  YSS'         S*S         YSSP~YSSY    S*S    SSS  S*S  SSS    S*S
#            SP   SP                SP                       SP          SP          SP
#            Y    Y                 Y                        Y           Y           Y
#


class Historia(models.Model):
    codi_pacient = models.OneToOneField(
        Demo,
        on_delete=models.CASCADE
        )
    tabac_primer_cop = models.PositiveSmallIntegerField(
        blank=True
        )
    tabac_max_abstinencia = models.PositiveSmallIntegerField(
        blank=True)
    tabac_dias_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(30)
            ]
        )
    tabac_consum_diari = models.PositiveSmallIntegerField(
        blank=True)
    tabac_via_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
            ]
        )
    tabac_detox_anterior = models.BooleanField(
        null = True
        )
    oh_primer_cop = models.PositiveSmallIntegerField(
        blank=True
        )
    oh_max_abstinencia = models.PositiveSmallIntegerField(
        blank=True
        )
    oh_dias_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(30)
            ]
        )
    oh_consum_diari = models.PositiveSmallIntegerField(
        blank=True
        )
    oh_via_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
            ]
        )
    oh_detox_anterior = models.BooleanField(
        null = True
        )
    thc_primer_cop = models.PositiveSmallIntegerField(
        blank=True
        )
    thc_max_abstinencia = models.PositiveSmallIntegerField(
        blank=True
        )
    thc_dias_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(30)
            ]
        )
    thc_consum_diari = models.PositiveSmallIntegerField(
        blank=True
        )
    thc_via_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
            ]
        )
    thc_detox_anterior = models.BooleanField(
        null = True
        )
    coc_primer_cop = models.PositiveSmallIntegerField(
        blank=True
        )
    coc_max_abstinencia = models.PositiveSmallIntegerField(
        blank=True
        )
    coc_dias_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(30)
            ]
        )
    coc_consum_diari = models.PositiveSmallIntegerField(
        blank=True
        )
    coc_via_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
            ]
        )
    coc_detox_anterior = models.BooleanField(
        null = True
        )
    amf_primer_cop = models.PositiveSmallIntegerField(
        blank=True
        )
    amf_max_abstinencia = models.PositiveSmallIntegerField(
        blank=True
        )
    amf_dias_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(30)
            ]
        )
    amf_consum_diari = models.PositiveSmallIntegerField(
        blank=True
        )
    amf_via_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
            ]
        )
    amf_detox_anterior = models.BooleanField(
        null = True
        )
    her_primer_cop = models.PositiveSmallIntegerField(
        blank=True
        )
    her_max_abstinencia = models.PositiveSmallIntegerField(
        blank=True
        )
    her_dias_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(30)
                ]
            )
    her_consum_diari = models.PositiveSmallIntegerField(
        blank=True
        )
    her_via_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
                ]
            )
    her_detox_anterior = models.BooleanField(
        null = True
        )
    bzd_primer_cop = models.PositiveSmallIntegerField(
        blank=True
        )
    bzd_max_abstinencia = models.PositiveSmallIntegerField(
        blank=True
        )
    bzd_dias_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(30)
                ]
            )
    bzd_consum_diari = models.PositiveSmallIntegerField(
        blank=True
        )
    bzd_via_consum = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
                ]
            )
    bzd_detox_anterior = models.BooleanField(
        null = True
        )
    tox_ppal = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
                ]
            )
    tus_altres = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
                ]
            )
    gravetat_tus = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
                ]
            )
    projecte_tt = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
                ]
            )
    reingres = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
                ]
            )
    cc_fisiques = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
                ]
            )
    cc_emocionals = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(7)
                ]
            )
    dg_alta = models.CharField(max_length=100)


    
    
    
#     8888888888 .d8888b.  .d8888b.        d8888888            d8888 .d8888b.
#     888       d88P  Y88bd88P  Y88b      d88888888           d88888d88P  Y88b
#     888       Y88b.     888    888     d88P888888          d88P888Y88b.
#     8888888    "Y888b.  888           d88P 888888         d88P 888 "Y888b.
#     888           "Y88b.888          d88P  888888        d88P  888    "Y88b.
#     888             "888888    888  d88P   888888       d88P   888      "888
#     888       Y88b  d88PY88b  d88P d8888888888888      d8888888888Y88b  d88P
#     8888888888 "Y8888P"  "Y8888P" d88P     88888888888d88P     888 "Y8888P"
#
#
#



class Escala(models.Model):
    codi_pacient = models.OneToOneField(
        Demo,
        on_delete=models.CASCADE
        )

#      .S_SSSs     .S_SSSs     .S_sSSs     .S_sSSs     .S_SSSs    sdSS_SSSSSSbs
#     .SS~SSSSS   .SS~SSSSS   .SS~YS%%b   .SS~YS%%b   .SS~SSSSS   YSSS~S%SSSSSP
#     S%S   SSSS  S%S   SSSS  S%S   `S%b  S%S   `S%b  S%S   SSSS       S%S
#     S%S    S%S  S%S    S%S  S%S    S%S  S%S    S%S  S%S    S%S       S%S
#     S%S SSSS%P  S%S SSSS%S  S%S    d*S  S%S    d*S  S%S SSSS%S       S&S
#     S&S  SSSY   S&S  SSS%S  S&S   .S*S  S&S   .S*S  S&S  SSS%S       S&S
#     S&S    S&S  S&S    S&S  S&S_sdSSS   S&S_sdSSS   S&S    S&S       S&S
#     S&S    S&S  S&S    S&S  S&S~YSY%b   S&S~YSY%b   S&S    S&S       S&S
#     S*S    S&S  S*S    S&S  S*S   `S%b  S*S   `S%b  S*S    S&S       S*S
#     S*S    S*S  S*S    S*S  S*S    S%S  S*S    S%S  S*S    S*S       S*S
#     S*S SSSSP   S*S    S*S  S*S    S&S  S*S    S&S  S*S    S*S       S*S
#     S*S  SSY    SSS    S*S  S*S    SSS  S*S    SSS  SSS    S*S       S*S
#     SP                 SP   SP          SP                 SP        SP
#     Y                  Y    Y           Y                  Y         Y
#
    barrat_01 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_02 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_03 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_04 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_05 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_06 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_07 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_08 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_09 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_10 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_11 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_12 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_13 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_14 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_15 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_16 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_17 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_18 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_19 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_20 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_21 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_22 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_23 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_24 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_25 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_26 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_27 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_28 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_29 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    barrat_30 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )

#      .S_SSSs    S.        sSSs   .S S.
#     .SS~SSSSS   SS.      d%%SP  .SS SS.
#     S%S   SSSS  S%S     d%S'    S%S S%S
#     S%S    S%S  S%S     S%S     S%S S%S
#     S%S SSSS%S  S&S     S&S     S%S S%S
#     S&S  SSS%S  S&S     S&S_Ss   SS SS
#     S&S    S&S  S&S     S&S~SP    S_S
#     S&S    S&S  S&S     S&S      SS~SS
#     S*S    S&S  S*b     S*b     S*S S*S
#     S*S    S*S  S*S.    S*S.    S*S S*S
#     S*S    S*S   SSSbs   SSSbs  S*S S*S
#     SSS    S*S    YSSP    YSSP  S*S S*S
#            SP                   SP
#            Y                    Y
#

    alex_01 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_02 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_03 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_04 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_05 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_06 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_07 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_08 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_09 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_10 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_11 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_12 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_13 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_14 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_15 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_16 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_17 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_18 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_19 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    alex_20 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
#      .S_sSSs      sSSs_sSSs      sSSs    sSSs   .S_SsS_S.    .S_SSSs      sSSs   .S_sSSs      sSSSSs
#     .SS~YS%%b    d%%SP~YS%%b    d%%SP   d%%SP  .SS~S*S~SS.  .SS~SSSSS    d%%SP  .SS~YS%%b    d%%%%SP
#     S%S   `S%b  d%S'     `S%b  d%S'    d%S'    S%S `Y' S%S  S%S   SSSS  d%S'    S%S   `S%b  d%S'
#     S%S    S%S  S%S       S%S  S%|     S%S     S%S     S%S  S%S    S%S  S%S     S%S    S%S  S%S
#     S%S    d*S  S&S       S&S  S&S     S&S     S%S     S%S  S%S SSSS%P  S&S     S%S    d*S  S&S
#     S&S   .S*S  S&S       S&S  Y&Ss    S&S_Ss  S&S     S&S  S&S  SSSY   S&S_Ss  S&S   .S*S  S&S
#     S&S_sdSSS   S&S       S&S  `S&&S   S&S~SP  S&S     S&S  S&S    S&S  S&S~SP  S&S_sdSSS   S&S
#     S&S~YSY%b   S&S       S&S    `S*S  S&S     S&S     S&S  S&S    S&S  S&S     S&S~YSY%b   S&S sSSs
#     S*S   `S%b  S*b       d*S     l*S  S*b     S*S     S*S  S*S    S&S  S*b     S*S   `S%b  S*b `S%%
#     S*S    S%S  S*S.     .S*S    .S*P  S*S.    S*S     S*S  S*S    S*S  S*S.    S*S    S%S  S*S   S%
#     S*S    S&S   SSSbs_sdSSS   sSS*S    SSSbs  S*S     S*S  S*S SSSSP    SSSbs  S*S    S&S   SS_sSSS
#     S*S    SSS    YSSP~YSSY    YSS'      YSSP  SSS     S*S  S*S  SSY      YSSP  S*S    SSS    Y~YSSY
#     SP                                                 SP   SP                  SP
#     Y                                                  Y    Y                   Y
#

    rosemberg_01 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(4)
                ]
            )
    rosemberg_02 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(4)
                ]
            )
    rosemberg_03 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(4)
                ]
            )
    rosemberg_04 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(4)
                ]
            )
    rosemberg_05 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(4)
                ]
            )
    rosemberg_06 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(4)
                ]
            )
    rosemberg_07 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(4)
                ]
            )
    rosemberg_08 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(4)
                ]
            )
    rosemberg_09 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(4)
                ]
            )
    rosemberg_10 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(4)
                ]
            )
#      .S    sSSs   .S_SsS_S.    .S
#     .SS   d%%SP  .SS~S*S~SS.  .SS
#     S%S  d%S'    S%S `Y' S%S  S%S
#     S%S  S%|     S%S     S%S  S%S
#     S&S  S&S     S%S     S%S  S&S
#     S&S  Y&Ss    S&S     S&S  S&S
#     S&S  `S&&S   S&S     S&S  S&S
#     S&S    `S*S  S&S     S&S  S&S
#     S*S     l*S  S*S     S*S  S*S
#     S*S    .S*P  S*S     S*S  S*S
#     S*S  sSS*S   S*S     S*S  S*S
#     S*S  YSS'    SSS     S*S  S*S
#     SP                   SP   SP
#     Y                    Y    Y
#
    ismi_01 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_02 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_03 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_04 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_05 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_06 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_07 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_08 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_09 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_10 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_11 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_12 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_13 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_14 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_15 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_16 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_17 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_18 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_19 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_20 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_21 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_22 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_23 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_24 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_25 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_26 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_27 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_28 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    ismi_29 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )

#       sSSs  S.       .S   .S_sSSs     .S    sSSs
#      d%%SP  SS.     .SS  .SS~YS%%b   .SS   d%%SP
#     d%S'    S%S     S%S  S%S   `S%b  S%S  d%S'
#     S%S     S%S     S%S  S%S    S%S  S%S  S%S
#     S&S     S&S     S&S  S%S    S&S  S&S  S&S
#     S&S     S&S     S&S  S&S    S&S  S&S  S&S
#     S&S     S&S     S&S  S&S    S&S  S&S  S&S
#     S&S     S&S     S&S  S&S    S&S  S&S  S&S
#     S*b     S*b     S*S  S*S    S*S  S*S  S*b
#     S*S.    S*S.    S*S  S*S    S*S  S*S  S*S.
#      SSSbs   SSSbs  S*S  S*S    S*S  S*S   SSSbs
#       YSSP    YSSP  S*S  S*S    SSS  S*S    YSSP
#                     SP   SP          SP
#                     Y    Y           Y
#

    clinic_tus = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    clinic_neccesitat = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    clinic_social = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    clinic_fam = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    clinic_fisiq = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    clinic_emo = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    clinic_diners = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    dsm_ctrl_1 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    dsm_ctrl_2 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    dsm_ctrl_3 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    dsm_ctrl_4 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    dsm_ctrl_5 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    dsm_fr = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    dsm_conseq_1 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    dsm_conseq_2 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    dsm_conseq_3 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    dsm_conseq_4 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    dsm_conseq_5 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    ic_self = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
    ic_internal = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)
                ]
            )
#      .S    S.    .S_SSSs     .S_SsS_S.    .S  S.
#     .SS    SS.  .SS~SSSSS   .SS~S*S~SS.  .SS  SS.
#     S%S    S%S  S%S   SSSS  S%S `Y' S%S  S%S  S%S
#     S%S    S%S  S%S    S%S  S%S     S%S  S%S  S%S
#     S%S SSSS%S  S%S SSSS%S  S%S     S%S  S&S  S&S
#     S&S  SSS&S  S&S  SSS%S  S&S     S&S  S&S  S&S
#     S&S    S&S  S&S    S&S  S&S     S&S  S&S  S&S
#     S&S    S&S  S&S    S&S  S&S     S&S  S&S  S&S
#     S*S    S*S  S*S    S&S  S*S     S*S  S*S  S*b
#     S*S    S*S  S*S    S*S  S*S     S*S  S*S  S*S.
#     S*S    S*S  S*S    S*S  S*S     S*S  S*S   SSSbs
#     SSS    S*S  SSS    S*S  SSS     S*S  S*S    YSSP
#            SP          SP           SP   SP
#            Y           Y            Y    Y
#

    hamil_pre_01 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_pre_02 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_pre_03 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_pre_04 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_pre_05 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_pre_06 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_pre_07 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_pre_08 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_pre_09 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_pre_10 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_pre_11 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    hamil_pre_12 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_pre_13 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_pre_14 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_pre_15 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_pre_16 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_pre_17 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_pre_18 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_pre_19 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_pre_20 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_post_01 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_post_02 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_post_03 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_post_04 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_post_05 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_post_06 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_post_07 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_post_08 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_post_09 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_post_10 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_post_11 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_post_12 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_post_13 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_post_14 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_post_15 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_post_16 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_post_17 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_post_18 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
    hamil_post_19 = models.SmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(-2),
            MaxValueValidator(0)
                ]
            )
    hamil_post_20 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2)
                ]
            )
#      .S       S.    .S_sSSs     .S    sSSs   .S_SSSs
#     .SS       SS.  .SS~YS%%b   .SS   d%%SP  .SS~SSSSS
#     S%S       S%S  S%S   `S%b  S%S  d%S'    S%S   SSSS
#     S%S       S%S  S%S    S%S  S%S  S%S     S%S    S%S
#     S&S       S&S  S%S    d*S  S&S  S&S     S%S SSSS%S
#     S&S       S&S  S&S   .S*S  S&S  S&S     S&S  SSS%S
#     S&S       S&S  S&S_sdSSS   S&S  S&S     S&S    S&S
#     S&S       S&S  S&S~YSY%b   S&S  S&S     S&S    S&S
#     S*b       d*S  S*S   `S%b  S*S  S*b     S*S    S&S
#     S*S.     .S*S  S*S    S%S  S*S  S*S.    S*S    S*S
#      SSSbs_sdSSS   S*S    S&S  S*S   SSSbs  S*S    S*S
#       YSSP~YSSY    S*S    SSS  S*S    YSSP  SSS    S*S
#                    SP          SP                  SP
#                    Y           Y                   Y
#
    urica_01 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_02 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_03 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_04 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_05 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_06 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_07 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_08 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_09 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_10 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_11 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_12 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_13 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_14 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_15 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_16 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_17 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_18 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_19 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_20 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_21 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_22 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_23 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_24 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_25 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_26 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_27 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_28 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_29 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_30 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_31 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    urica_32 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
#      .S_SSSs      sSSs   .S    sSSs
#     .SS~SSSSS    d%%SP  .SS   d%%SP
#     S%S   SSSS  d%S'    S%S  d%S'
#     S%S    S%S  S%S     S%S  S%|
#     S%S SSSS%P  S&S     S&S  S&S
#     S&S  SSSY   S&S     S&S  Y&Ss
#     S&S    S&S  S&S     S&S  `S&&S
#     S&S    S&S  S&S     S&S    `S*S
#     S*S    S&S  S*b     S*S     l*S
#     S*S    S*S  S*S.    S*S    .S*P
#     S*S SSSSP    SSSbs  S*S  sSS*S
#     S*S  SSY      YSSP  S*S  YSS'
#     SP                  SP
#     Y                   Y
#
    bcis_pre_01 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_02 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_03 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_04 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_05 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_06 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_07 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_08 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_09 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_10 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_11 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_12 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_13 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_14 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_pre_15 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_01 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_02 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_03 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_04 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_05 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_06 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_07 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_08 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_09 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_10 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_11 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_12 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_13 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_14 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    bcis_post_15 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
#       sSSs   .S_sSSs     .S_SSSs      sSSs    sSSs
#      d%%SP  .SS~YS%%b   .SS~SSSSS    d%%SP   d%%SP
#     d%S'    S%S   `S%b  S%S   SSSS  d%S'    d%S'
#     S%S     S%S    S%S  S%S    S%S  S%|     S%S
#     S&S     S%S    d*S  S%S SSSS%P  S&S     S&S
#     S&S_Ss  S&S   .S*S  S&S  SSSY   Y&Ss    S&S_Ss
#     S&S~SP  S&S_sdSSS   S&S    S&S  `S&&S   S&S~SP
#     S&S     S&S~YSY%b   S&S    S&S    `S*S  S&S
#     S*b     S*S   `S%b  S*S    S&S     l*S  S*b
#     S*S     S*S    S%S  S*S    S*S    .S*P  S*S.
#     S*S     S*S    S&S  S*S SSSSP   sSS*S    SSSbs
#     S*S     S*S    SSS  S*S  SSY    YSS'      YSSP
#     SP      SP          SP
#     Y       Y           Y
#
    frbse_pac_apa_pre = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    frbse_pac_apa_post = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    frbse_pac_desinh_pre = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    frbse_pac_desinh_post = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    frbse_pac_exe_pre = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    frbse_pac_exe_post = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    frbse_inf_apa_pre = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    frbse_inf_apa_post = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    frbse_inf_desinh_pre = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    frbse_inf_desinh_post = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    frbse_inf_exe_pre = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    frbse_inf_exe_post = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )

#      .S_SSSs     .S_sSSs     .S
#     .SS~SSSSS   .SS~YS%%b   .SS
#     S%S   SSSS  S%S   `S%b  S%S
#     S%S    S%S  S%S    S%S  S%S
#     S%S SSSS%P  S%S    S&S  S&S
#     S&S  SSSY   S&S    S&S  S&S
#     S&S    S&S  S&S    S&S  S&S
#     S&S    S&S  S&S    S&S  S&S
#     S*S    S&S  S*S    d*S  S*S
#     S*S    S*S  S*S   .S*S  S*S
#     S*S SSSSP   S*S_sdSSS   S*S
#     S*S  SSY    SSS~YSSY    S*S
#     SP                      SP
#     Y                       Y
# 
    bdi_pre = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    bdi_post = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
#      .S_SSSs     .S_SSSs     .S
#     .SS~SSSSS   .SS~SSSSS   .SS
#     S%S   SSSS  S%S   SSSS  S%S
#     S%S    S%S  S%S    S%S  S%S
#     S%S SSSS%P  S%S SSSS%S  S&S
#     S&S  SSSY   S&S  SSS%S  S&S
#     S&S    S&S  S&S    S&S  S&S
#     S&S    S&S  S&S    S&S  S&S
#     S*S    S&S  S*S    S&S  S*S
#     S*S    S*S  S*S    S*S  S*S
#     S*S SSSSP   S*S    S*S  S*S
#     S*S  SSY    SSS    S*S  S*S
#     SP                 SP   SP
#     Y                  Y    Y
#

    bai_pre = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    bai_post = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )

#      .S_SsS_S.    .S_SSSs     .S_sSSs     .S_sSSs      sSSs
#     .SS~S*S~SS.  .SS~SSSSS   .SS~YS%%b   .SS~YS%%b    d%%SP
#     S%S `Y' S%S  S%S   SSSS  S%S   `S%b  S%S   `S%b  d%S'
#     S%S     S%S  S%S    S%S  S%S    S%S  S%S    S%S  S%|
#     S%S     S%S  S%S SSSS%S  S%S    S&S  S%S    d*S  S&S
#     S&S     S&S  S&S  SSS%S  S&S    S&S  S&S   .S*S  Y&Ss
#     S&S     S&S  S&S    S&S  S&S    S&S  S&S_sdSSS   `S&&S
#     S&S     S&S  S&S    S&S  S&S    S&S  S&S~YSY%b     `S*S
#     S*S     S*S  S*S    S&S  S*S    d*S  S*S   `S%b     l*S
#     S*S     S*S  S*S    S*S  S*S   .S*S  S*S    S%S    .S*P
#     S*S     S*S  S*S    S*S  S*S_sdSSS   S*S    S&S  sSS*S
#     SSS     S*S  SSS    S*S  SSS~YSSY    S*S    SSS  YSS'
#             SP          SP               SP
#             Y           Y                Y
#
    madrs_pre = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    madrs_post = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )

#      .S_SSSs     .S_sSSs     .S_sSSs      sSSs
#     .SS~SSSSS   .SS~YS%%b   .SS~YS%%b    d%%SP
#     S%S   SSSS  S%S   `S%b  S%S   `S%b  d%S'
#     S%S    S%S  S%S    S%S  S%S    S%S  S%|
#     S%S SSSS%P  S%S    d*S  S%S    d*S  S&S
#     S&S  SSSY   S&S   .S*S  S&S   .S*S  Y&Ss
#     S&S    S&S  S&S_sdSSS   S&S_sdSSS   `S&&S
#     S&S    S&S  S&S~YSSY    S&S~YSY%b     `S*S
#     S*S    S&S  S*S         S*S   `S%b     l*S
#     S*S    S*S  S*S         S*S    S%S    .S*P
#     S*S SSSSP   S*S         S*S    S&S  sSS*S
#     S*S  SSY    S*S         S*S    SSS  YSS'
#     SP          SP          SP
#     Y           Y           Y
#
    bprs_pos_pre = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    bprs_pos_post = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    bprs_neg_pre = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    bprs_neg_post = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
#      .S    S.     sSSs_sSSs     .S_sSSs      sSSs_sSSs      sSSs
#     .SS    SS.   d%%SP~YS%%b   .SS~YS%%b    d%%SP~YS%%b    d%%SP
#     S%S    S%S  d%S'     `S%b  S%S   `S%b  d%S'     `S%b  d%S'
#     S%S    S%S  S%S       S%S  S%S    S%S  S%S       S%S  S%|
#     S%S SSSS%S  S&S       S&S  S%S    S&S  S&S       S&S  S&S
#     S&S  SSS&S  S&S       S&S  S&S    S&S  S&S       S&S  Y&Ss
#     S&S    S&S  S&S       S&S  S&S    S&S  S&S       S&S  `S&&S
#     S&S    S&S  S&S       S&S  S&S    S&S  S&S       S&S    `S*S
#     S*S    S*S  S*b       d*S  S*S    S*S  S*b       d*S     l*S
#     S*S    S*S  S*S.     .S*S  S*S    S*S  S*S.     .S*S    .S*P
#     S*S    S*S   SSSbs_sdSSS   S*S    S*S   SSSbs_sdSSS   sSS*S
#     SSS    S*S    YSSP~YSSY    S*S    SSS    YSSP~YSSY    YSS'
#            SP                  SP
#            Y                   Y
#

    honos_pre = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    honos_post = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )

#      .S_SsS_S.    .S_SSSs     .S_sSSs     .S    S.     sSSs   .S   .S_SsS_S.
#     .SS~S*S~SS.  .SS~SSSSS   .SS~YS%%b   .SS    SS.   d%%SP  .SS  .SS~S*S~SS.
#     S%S `Y' S%S  S%S   SSSS  S%S   `S%b  S%S    S%S  d%S'    S%S  S%S `Y' S%S
#     S%S     S%S  S%S    S%S  S%S    S%S  S%S    S%S  S%S     S%S  S%S     S%S
#     S%S     S%S  S%S SSSS%S  S%S    S&S  S%S SSSS%S  S&S     S&S  S%S     S%S
#     S&S     S&S  S&S  SSS%S  S&S    S&S  S&S  SSS&S  S&S_Ss  S&S  S&S     S&S
#     S&S     S&S  S&S    S&S  S&S    S&S  S&S    S&S  S&S~SP  S&S  S&S     S&S
#     S&S     S&S  S&S    S&S  S&S    S&S  S&S    S&S  S&S     S&S  S&S     S&S
#     S*S     S*S  S*S    S&S  S*S    S*S  S*S    S*S  S*b     S*S  S*S     S*S
#     S*S     S*S  S*S    S*S  S*S    S*S  S*S    S*S  S*S.    S*S  S*S     S*S
#     S*S     S*S  S*S    S*S  S*S    S*S  S*S    S*S   SSSbs  S*S  S*S     S*S
#     SSS     S*S  SSS    S*S  S*S    SSS  SSS    S*S    YSSP  S*S  SSS     S*S
#             SP          SP   SP                 SP           SP           SP
#             Y           Y    Y                  Y            Y            Y
#

    manheim_pre_1_a_12 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    manheim_pre_13 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    manheim_pre_14 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    manheim_pre_15 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    manheim_pre_16 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    manheim_post_1_a_12 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    manheim_post_13 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    manheim_post_14 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    manheim_post_15 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    manheim_post_16 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )

#       sSSs_sSSs     .S_SSSs      sSSs    sSSs
#      d%%SP~YS%%b   .SS~SSSSS    d%%SP   d%%SP
#     d%S'     `S%b  S%S   SSSS  d%S'    d%S'
#     S%S       S%S  S%S    S%S  S%S     S%|
#     S&S       S&S  S%S SSSS%P  S&S     S&S
#     S&S       S&S  S&S  SSSY   S&S     Y&Ss
#     S&S       S&S  S&S    S&S  S&S     `S&&S
#     S&S       S&S  S&S    S&S  S&S       `S*S
#     S*b       d*S  S*S    S&S  S*b        l*S
#     S*S.     .S*S  S*S    S*S  S*S.      .S*P
#      SSSbs_sdSSSS  S*S SSSSP    SSSbs  sSS*S
#       YSSP~YSSSSS  S*S  SSY      YSSP  YSS'
#                    SP
#                    Y
#

    qbcs_01 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    qbcs_02 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    qbcs_03 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    qbcs_04 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    qbcs_05 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    qbcs_06 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    qbcs_07 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    qbcs_08 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    trauma = models.BooleanField(null=True)
    sintomas = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
                ]
            )
    covid = models.BooleanField(null=True)


#       sSSs   .S_SSSs    sdSS_SSSSSSbs   .S    sSSs
#      d%%SP  .SS~SSSSS   YSSS~S%SSSSSP  .SS   d%%SP
#     d%S'    S%S   SSSS       S%S       S%S  d%S'
#     S%|     S%S    S%S       S%S       S%S  S%|
#     S&S     S%S SSSS%S       S&S       S&S  S&S
#     Y&Ss    S&S  SSS%S       S&S       S&S  Y&Ss
#     `S&&S   S&S    S&S       S&S       S&S  `S&&S
#       `S*S  S&S    S&S       S&S       S&S    `S*S
#        l*S  S*S    S&S       S*S       S*S     l*S
#       .S*P  S*S    S*S       S*S       S*S    .S*P
#     sSS*S   S*S    S*S       S*S       S*S  sSS*S
#     YSS'    SSS    S*S       S*S       S*S  YSS'
#                    SP        SP        SP
#                    Y         Y         Y
#

    satis_01 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    satis_02 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    satis_03 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    satis_04 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    satis_05 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    satis_06 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    satis_07 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
    satis_08 = models.PositiveSmallIntegerField(
        blank=True,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5)
                ]
            )
